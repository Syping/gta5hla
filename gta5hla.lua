#!/usr/bin/env luaengine
--[[
******************************************************************************
* gta5hla GTA V Hardlink Assistant
* Copyright (C) 2019-2020 Syping
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
******************************************************************************
--]]

local mainWindow
local hardlinkOpt1
local hardlinkOpt2
local hardlinkOpt3
local sourceLineEdit
local targetLineEdit
local hardlinkButton
local sourceToolButton
local targetToolButton
-- local progressBarMeter
local progressTextView

local sourceTable = {}
local targetTable = {}

function main()
    mainWindow = createMainWindow("GTA V Hardlink Assistant (gta5hla)")
    local mainLayout = createLayout(VerticalLayout, mainWindow)

    createLabel("<b>Hardlink mode</b>", mainLayout)
    hardlinkOpt1 = createRadioButton("Hardlink different versions", mainLayout)
    setWidgetChecked(hardlinkOpt1, true)
    hardlinkOpt2 = createRadioButton("Hardlink different versions (without DLC)", mainLayout)
    setWidgetEnabled(hardlinkOpt2, false)
    hardlinkOpt3 = createRadioButton("Hardlink same versions (read tooltip)", mainLayout)
    setWidgetEnabled(hardlinkOpt3, false)

    createLabel("<b>GTA V Paths</b>", mainLayout)

    local sourceLayout = createLayout(HorizontalLayout, mainLayout)
    createLabel("Source:", sourceLayout)
    sourceLineEdit = createLineEdit("", sourceLayout)
    setWidgetReadOnly(sourceLineEdit)
    sourceToolButton = createToolButton("...", sourceLayout)
    connect(sourceToolButton, "clicked()", "sourceButtonPressed")

    local targetLayout = createLayout(HorizontalLayout, mainLayout)
    createLabel("Target:", targetLayout)
    targetLineEdit = createLineEdit("", targetLayout)
    setWidgetReadOnly(targetLineEdit)
    targetToolButton = createToolButton("...", targetLayout)
    connect(targetToolButton, "clicked()", "targetButtonPressed")
    setWidgetEnabled(targetToolButton, false)

    createLabel("<b>Progress</b>", mainLayout)
    progressTextView = createTextEdit("", mainLayout)
    setWidgetReadOnly(progressTextView)
    -- progressBarMeter = createProgressBar(0, mainLayout)
    -- setObjectText(progressBarMeter, "")

    local buttonLayout = createLayout(HorizontalLayout, mainLayout)
    hardlinkButton = createPushButton("&Hardlink", buttonLayout)
    connect(hardlinkButton, "clicked()", "hardlinkFiles")
    setWidgetEnabled(hardlinkButton, false)
    local syncButton = createPushButton("&Sync", buttonLayout)
    setWidgetEnabled(syncButton, false)
    createSpacerItem(SizePolicyExpanding, SizePolicyMinimum, buttonLayout)
    local closeButton = createPushButton("&Close", buttonLayout)
    connect(closeButton, "clicked()", "closeWindow")

    setWidgetSize(mainWindow, 500, 450) 
    setWidgetFixed(mainWindow, true)
    showWidget(mainWindow, ShowDefault)

    return GuiExecuted
end

function closeWindow()
    closeWidget(mainWindow)
end

function sourceButtonPressed()
    local folderPath = showFileDialog(OpenFolderDialog, "Select Source Folder...", mainWindow)
    if (folderPath ~= nil) then
        folderPath = string.gsub(folderPath, "/", "\\")
        if (scanSourceFolder(folderPath)) then
            setObjectText(sourceLineEdit, folderPath)
            setWidgetEnabled(targetToolButton, true)
        else
            showMessageBox(WarningMessageBox, "The selected Source folder is not valid!", "Select Source Folder...", mainWindow)
        end
    else
        setObjectText(sourceLineEdit, "")
        setObjectText(targetLineEdit, "")
        setWidgetEnabled(hardlinkButton, false)
        setWidgetEnabled(targetToolButton, false)
    end
end

function targetButtonPressed()
    local folderPath = showFileDialog(OpenFolderDialog, "Select Target Folder...", mainWindow)
    if (folderPath ~= nil) then
        folderPath = string.gsub(folderPath, "/", "\\")
        if (scanTargetFolder(folderPath)) then
            setObjectText(targetLineEdit, folderPath)
            setWidgetEnabled(hardlinkButton, true)
        else
            showMessageBox(WarningMessageBox, "The selected Target folder is not valid!", "Select Target Folder...", mainWindow)
        end
    else
        setObjectText(targetLineEdit, "")
        setWidgetEnabled(hardlinkButton, false)
    end
end

function scanSourceFolder(folderPath)
    sourceTable = {}
    widgetAddText(progressTextView, "<b>Source files</b>")
    for index, file in ipairs(directoryListFiles(folderPath)) do
        local fileName = string.match(file, '.*%.rpf$')
        if (fileName ~= nil) then
            if (string.match(fileName, '.*update.rpf$') == nil) then
                fileName = string.sub(fileName, string.len(folderPath)+2)
                table.insert(sourceTable, fileName)
                widgetAddText(progressTextView, fileName)
            end
        end
    end
    if (next(sourceTable) == nil) then
        return false
    end
    return true
end

function scanTargetFolder(folderPath)
    targetTable = {}
    widgetAddText(progressTextView, "<b>Target files</b>")
    for index, file in ipairs(directoryListFiles(folderPath)) do
        local fileName = string.match(file, '.*%.rpf$')
        if (fileName ~= nil) then
            if (string.match(fileName, '.*update.rpf$') == nil) then
                fileName = string.sub(fileName, string.len(folderPath)+2)
                table.insert(targetTable, fileName)
                widgetAddText(progressTextView, fileName)
            end
        end
    end
    if (next(targetTable) == nil) then
        return false
    end
    return true
end

function hardlinkFiles()
    setWidgetEnabled(mainWindow, false)
    widgetAddText(progressTextView, "<b>Hardlinking...</b>")
    for index, file in ipairs(sourceTable) do
        local sourceFile = getObjectText(sourceLineEdit).."\\"..file
        local targetFile = getObjectText(targetLineEdit).."\\"..file
        local fileHandle = io.open(targetFile, "r")
        if (fileHandle ~= nil) then
            io.close(fileHandle)
        else
            widgetAddText(progressTextView, "<span style=\"color: red\">Failed to open "..targetFile.."</span>")
            setWidgetEnabled(mainWindow, true)
            return
        end
        if (os.remove(targetFile)) then
            widgetAddText(progressTextView, "<span style=\"color: green\">"..targetFile.." removed</span>")
            updateUi()
            if (linkFile(sourceFile, targetFile, Hardlink)) then
                widgetAddText(progressTextView, "<span style=\"color: green\">"..targetFile.." hardlinked</span>")
                updateUi()
            else
                widgetAddText(progressTextView, "<span style=\"color: red\">Failed to hardlink "..targetFile.."</span>")
                setWidgetEnabled(mainWindow, true)
                return
            end
        else
            widgetAddText(progressTextView, "<span style=\"color: red\">Failed to remove "..targetFile.."</span>")
            setWidgetEnabled(mainWindow, true)
            return
        end
    end
    setWidgetEnabled(mainWindow, true)
end
